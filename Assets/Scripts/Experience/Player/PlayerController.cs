using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Move camera by mouse
    [Header("Move Camera")]
    [SerializeField] private Transform playerCamera;
    [SerializeField] private Transform cameraHolder;
    [SerializeField] private float senX;
    [SerializeField] private float senY;
    [SerializeField] private Transform orientation;
    private float xRotation;
    private float yRotation;
    // Player Movement
    [Header("Player Movement")]
    [SerializeField] private float moveSpeed;
    [SerializeField] private LayerMask isGround;
    [SerializeField] private float PlayerHeight;
    private Vector3 moveDirection;
    private float horizontalInput;
    private float verticalInput;
    private bool grounded;
    private Rigidbody playerRigidbody;
    // Pickup and drop
    [SerializeField] private float maxDistancePickUp = 5f;
    [SerializeField] private GameObject itemHolder;
    private GameObject currentObjectHolding;
    private bool isHolding;
    private bool isSafetyPinRemoved;
    // Control Fire Extinguisher
    private FireExtinguisherController fireExtinguisherController;
    private void Start()
    {
        playerRigidbody = GetComponent<Rigidbody>();
        playerRigidbody.freezeRotation = true;
        // Lock cursor when play scenen
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {
        MovePlayerCamera();
        CameraMovement();
        InputMovePlayer();
        GroundCheck();
        LimitSpeed();
        if(Input.GetKeyDown("e"))
        {
            Debug.Log("Key E is pressed");
            PickUp();
        }
        else if (Input.GetKeyDown("q"))
        {
            Debug.Log("Key Q is pressed");
            Drop();
        }

        if (Input.GetKeyDown("r") && isHolding == true)
        {
            Debug.Log("Key R is pressed");
            ReleasePin();

        }

        if(Input.GetMouseButtonDown(0) && isHolding == true && isSafetyPinRemoved == true) 
        {
            Debug.Log("Pressed Left Click !!!");
            PlayFireExtinguisher();
        }
        else if(Input.GetMouseButtonUp(0) && isHolding == true && isSafetyPinRemoved == true)
        {
            Debug.Log("Release Left....");
            StopFireExtinguisher();
        }
    }

    private void ReleasePin()
    {
        Vector3 startLocalPosition = new Vector3(0, 0, 0);
        Vector3 endLocalPosition = new Vector3(-0.005f, 0, 0);
        currentObjectHolding.transform.GetChild(0).GetChild(3).transform.localPosition = Vector3.Lerp(startLocalPosition, endLocalPosition, 1f);
        isSafetyPinRemoved = true;
    }
    private void FixedUpdate()
    {
        MovePlayer();
    }
    private void CameraMovement()
    {
        cameraHolder.position = playerCamera.position;
    }
    private void MovePlayerCamera()
    {
        // Get mouse input
        float mouseX = Input.GetAxisRaw("Mouse X") * Time.deltaTime * senX;
        float mouseY = Input.GetAxisRaw("Mouse Y") * Time.deltaTime * senY;
        yRotation += mouseX;
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);
        // Rotation player camera and orientation
        playerCamera.transform.rotation = Quaternion.Euler(xRotation, yRotation, 0);
        orientation.transform.rotation = Quaternion.Euler(0, yRotation, 0);
    }
    private void InputMovePlayer()
    {
        horizontalInput = Input.GetAxisRaw("Horizontal");
        verticalInput = Input.GetAxisRaw("Vertical");
    }
    private void GroundCheck()
    {
        grounded = Physics.Raycast(transform.position, Vector3.down, PlayerHeight * 0.5f + 0.3f, isGround);
    }
    private void MovePlayer()
    {
        // Calculate movement direction
        moveDirection = orientation.forward * verticalInput + orientation.right * horizontalInput;
        if(grounded)
        {
            playerRigidbody.AddForce(moveDirection.normalized * 10f * moveSpeed, ForceMode.Force);
        }
    }
    private void LimitSpeed()
    {
        Vector3 currentSpeed = new Vector3(playerRigidbody.velocity.x, 0f, playerRigidbody.velocity.z);
        if(currentSpeed.magnitude > moveSpeed)
        {
            Vector3 newSpeed = currentSpeed.normalized * moveSpeed;
            playerRigidbody.velocity = new Vector3(newSpeed.x, playerRigidbody.velocity.y, newSpeed.z);
        }
    }
    private void PickUp()
    {
        RaycastHit hit;
        if(Physics.Raycast(playerCamera.transform.position, playerCamera.transform.forward, out hit, maxDistancePickUp))
        {
            if(hit.transform.tag == "FireExtinguisher")
            {
                Debug.Log("Catch the FireExtinguisher !!!!");
                fireExtinguisherController = hit.transform.GetComponentInChildren<FireExtinguisherController>();
                currentObjectHolding = hit.transform.gameObject;
                foreach(var colli in hit.transform.GetComponentsInChildren<Collider>())
                {
                    if(colli != null)
                    {
                        colli.enabled = false;
                    }
                }
                foreach(var rigid in hit.transform.GetComponentsInChildren<Rigidbody>())
                {
                    if(rigid != null)
                    {
                        rigid.isKinematic = true;
                    }
                }
                currentObjectHolding.transform.parent = itemHolder.transform;
                currentObjectHolding.transform.localPosition = Vector3.zero;
                currentObjectHolding.transform.localEulerAngles = Vector3.zero;
                isHolding = true;
            }
        }
    }
    private void Drop()
    {
        if(isHolding)
        {
            fireExtinguisherController = null;
            currentObjectHolding.transform.parent = null;
            foreach (var colli in currentObjectHolding.GetComponentsInChildren<Collider>())
            {
                if (colli != null)
                {
                    colli.enabled = true;
                }
            }
            foreach (var rigid in currentObjectHolding.GetComponentsInChildren<Rigidbody>())
            {
                if (rigid != null)
                {
                    rigid.isKinematic = false;
                }
            }
            isHolding = false;
            RaycastHit hitDown;
            Physics.Raycast(transform.position, -Vector3.up, out hitDown);
            currentObjectHolding.transform.position = hitDown.point + new Vector3(itemHolder.transform.forward.x, 0,  itemHolder.transform.forward.z);
        }
        else
        {
            return;
        }
    }
    private void PlayFireExtinguisher()
    {
        if(fireExtinguisherController != null)
        {
            fireExtinguisherController.FireExtinguisherPS.Play();
        }
    }
    private void StopFireExtinguisher()
    {
        if (fireExtinguisherController != null)
        {
            fireExtinguisherController.FireExtinguisherPS.Stop();
        }
    }
}
